from setuptools import setup, find_packages
setup(name="genfem",
      version="1.2",
      author="Louis Poirel",
      author_email="louis.poirel@m4x.org",
      description="a fast and easy way to generate finite element matrices",
      url="https://gitlab.inria.fr/solverstack/genfem",
      license="CeCILL-C",
      packages=find_packages(),
      entry_points={'console_scripts': ['genfem=genfem:entry_point']},
      py_modules=["genfem"]
)
